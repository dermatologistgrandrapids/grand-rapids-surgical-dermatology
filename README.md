**Grand rapids surgical dermatology**

Welcome to Surgical Dermatology in Grand Rapids. 
We are a dermatological medical practice located in Grand Rapids that specializes on skin cancer, dermatological surgery, and cosmetic processes. 
We want you to feel as if you are with family when you attend our practice. 
This is why our mission is to provide you with the best possible treatment we can get.
Please Visit Our Website [Grand rapids surgical dermatology](https://dermatologistgrandrapids.com/surgical-dermatology.php) for more information. 
---

## Our surgical dermatology in Grand rapids

Surgical dermatology in Grand Rapids is a specialized, highly effective procedure for skin cancer removal. 
The method was developed at the University of Wisconsin by Dr. Frederic Mohs in the 1930s and is now carried out worldwide.
Grand rapids surgical dermatology differs from most therapies for skin cancer in that it provides for direct and full microscopic analysis to remove cancerous tissue, 
so that the 'roots' and cancer extensions can be removed.
Surgical dermatology in Grand Rapids has been recognised as the skin cancer procedure with the best recorded cure rate due to the 
methodical way in which tissue is removed and tested.



